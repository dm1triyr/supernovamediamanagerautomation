package favorites;

import base.TestBase;
import extentreportlistener.TestNGListener;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.FavoritePage;
import pages.HeaderNavigationPage;
import pages.UserProfilePage;

import java.lang.reflect.Method;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

@Listeners(TestNGListener.class)
public class FavoriteTest extends TestBase {
    Method method;
    ITestContext context;
    FavoritePage favoritePage;
    HeaderNavigationPage headerNavigation;

    public FavoriteTest(){
        super();
    }

    @BeforeMethod
    public void setUp(Method m, ITestContext c) throws Exception {
        method = m;
        context = c;
        String methodName = method.getName();
        initialization("FavoriteTestAutomation", methodName);
        headerNavigation = new HeaderNavigationPage(driver);
        context.setAttribute("WebDriver", driver);
        favoritePage = headerNavigation.getFavoritePage();
    }

    //string content name to be passed into dynamic xpath
    private final String contentName = "bird";

    @Test(priority = 1)
    public void validateItemInFavorites() {
        try {
            Boolean isItemInFavorites = favoritePage.isItemInFavorites2(contentName);

            if (isItemInFavorites){
                String message = "Test Passed: Item is in Favorites";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 2)
    public void removeItemFromFavorites() {
        try {
            Boolean removeItemFromFavorites = favoritePage.removeItemFromFavorite(contentName);

            if (removeItemFromFavorites){
                String message = "Test Passed: Item removed from favorites";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 3)
    public void isAddToFavoritesPopupDisplayed() {
        try {
            Boolean isAddToFavoritesPopupDisplayed = favoritePage.isAddToFavoritesPopupDisplayed(contentName);

            if (isAddToFavoritesPopupDisplayed) {
                String message = "Test Passed: Added to favorites popup displayed";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 4)
    public void isRemoveFromFavoritesPopupDisplayed() {
        try {
            Boolean isRemoveFromFavoritesPopupDisplayed = favoritePage.isRemoveFromFavoritesPopupDisplayed(contentName);

            if (isRemoveFromFavoritesPopupDisplayed) {
                String message = "Test Passed: Removed from favorites popup displayed";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 5)
    public void undoAddToFavorites() {
        try {
            Boolean undoAddToFavorites = favoritePage.undoAddToFavorites(contentName);

            if (undoAddToFavorites) {
                String message = "Test Passed: Undo add to favorites button is functional";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex) {
            fail(ex.getMessage());
        }
    }

    //---Tests to add Later---
    //undo remove from favorites

    //can add item from library by hovering over it

    //add multiple items / remove multiple items with the green selector btn

    //select and remove multiple items with the "select all" and "unselect all" btns

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
