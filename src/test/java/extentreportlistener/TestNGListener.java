package extentreportlistener;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import base.TestBase;



public class TestNGListener implements ITestListener {
    WebDriver driver = null;

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        ITestContext context = result.getTestContext();
        String message = (String)context.getAttribute(result.getName());
        System.out.println(result.getName() + " " + message);

        driver = (WebDriver) context.getAttribute("WebDriver");

        if (driver != null) {
            TestBase.sendToBrowseStackTestStatus(driver,"passed", result.getName() + " " + message);
        }
    }

    @Override
    public void onTestFailure(ITestResult result) {
        String error = result.getThrowable().getMessage();
        System.out.println(result.getName() + " was a failure.\n Error: " + error);
        ITestContext context = result.getTestContext();
        driver = (WebDriver) context.getAttribute("WebDriver");
        error = error.replaceAll("[^a-zA-Z0-9]", " ");
        if (driver != null) {
            TestBase.sendToBrowseStackTestStatus(driver,"failed", error);
        }
    }



    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
