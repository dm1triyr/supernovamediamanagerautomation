package basket;

import base.TestBase;
import extentreportlistener.TestNGListener;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.BasketPage;
import pages.HeaderNavigationPage;

import java.lang.reflect.Method;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;


@Listeners(TestNGListener.class)
public class BasketTest extends TestBase {
    Method method;
    ITestContext context;
    BasketPage basketPage;
    HeaderNavigationPage headerNavigation;

    public BasketTest(){
        super();
    }

    @BeforeMethod
    public void setUp(Method m, ITestContext c) throws Exception {
        method = m;
        context = c;
        String methodName = method.getName();
        initialization("BasketTestAutomation", methodName);
        headerNavigation = new HeaderNavigationPage(driver);
        context.setAttribute("WebDriver", driver);
        basketPage = headerNavigation.getBasketPage();
    }

    @Test(priority = 1)
    public void itemssInBasket() {
        try {
            String mediaTitle = basketPage.AddItemToBasketLocalStorage();
            Boolean isItemInBasket = basketPage.isItemInBasket(mediaTitle);

            if (isItemInBasket) {
                String message = "Test Passed: Item added to basket";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }

        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 2)
    public void removeItemFromBasketHover() {
        try {
            String mediaTitle = basketPage.AddItemToBasketLocalStorage();
            Boolean isItemRemoved = basketPage.itemRemovedHover(mediaTitle);

            if (isItemRemoved) {
                String message = "Test Passed: Item removed from basket";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }

        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }



    //---Possible Tests to add---
    //validate item name matches when adding / removing

    //validate correct text in popup is displayed when adding and removing

    //validate can remove items from basket page not just from in item

    //can undo "add to basket" and undo "remove from basket"
    //

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
