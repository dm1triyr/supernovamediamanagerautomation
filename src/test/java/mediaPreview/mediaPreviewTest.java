package mediaPreview;

import extentreportlistener.TestNGListener;
import org.testng.ITestContext;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

import pages.*;
import base.TestBase;

import java.lang.reflect.Method;

@Listeners(TestNGListener.class)
public class mediaPreviewTest extends TestBase{
    Method method;
    ITestContext context;
    UserProfilePage userProfilePage;
    HeaderNavigationPage headerNavigation;

    public mediaPreviewTest(){
        super();
    }

    @BeforeMethod
    public void setUp(Method m, ITestContext c) throws Exception {
        method = m;
        context = c;
        String methodName = method.getName();
        initialization("MediaPreviewTestAutomation", methodName);
        headerNavigation = new HeaderNavigationPage(driver);
        context.setAttribute("WebDriver", driver);
        userProfilePage = headerNavigation.getUserProfilePage();
    }

}
