package home;

import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

import pages.*;
import base.TestBase;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;

import util.TestUtil;

public class HomeTest extends TestBase {
    Method method;
    ITestContext context;
    HomePage homePage;
    HeaderNavigationPage headerNavigation;
    String assetName = "winter stag";
    public HomeTest(){
        super();
    }

    @BeforeMethod
    public void setUp(Method m, ITestContext c) throws Exception {
        method = m;
        context = c;
        String methodName = method.getName();
        initialization("HomePageTestAutomation", methodName);
        headerNavigation = new HeaderNavigationPage(driver);
        context.setAttribute("WebDriver", driver);
        homePage = headerNavigation.getHomePage();
    }

    //test add item to basket and check if added item exists in basket
    @Test(priority = 1)
    public void addAssetToBasket() {
        try {
            Boolean addAsset = homePage.addAssetToBasket(assetName);

            if (addAsset) {
                String message = "Test Passed: Item added to basket from library hover tile";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }

        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }



    //test remove item from basket and check if removed item not exists anymore in basket
    @Test(priority = 2)
    public void removeItemFromBasket() {
        try {
            BasketPage basketPage = new BasketPage(driver);
            String mediaTitle = basketPage.AddItemToBasketLocalStorage();

            Boolean isItemInBasket = homePage.removeFromBasket(mediaTitle);

            if (isItemInBasket) {
                String message = "Test Passed: Item added to basket";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }

        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    //test click undo button in popup when user will try add item to basket
    @Test(priority = 3)
    public void undoAddItemToBasket() {
        try {


        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    //test click undo button in popup when user will try to remove item from basket
    @Test(priority = 4)
    public void undoRemoveItemToBasket() {
        try {


        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }


    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
