package userProfile;
import extentreportlistener.TestNGListener;
import org.testng.ITestContext;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

import pages.*;
import base.TestBase;

import java.lang.reflect.Method;

@Listeners(TestNGListener.class)
public class UserProfileTest extends TestBase{
    Method method;
    ITestContext context;
    UserProfilePage userProfilePage;
    HeaderNavigationPage headerNavigation;

    public UserProfileTest(){
        super();
    }

    @BeforeMethod
    public void setUp(Method m, ITestContext c) throws Exception {
        method = m;
        context = c;
        String methodName = method.getName();
        initialization("UserProfileTestAutomation", methodName);
        headerNavigation = new HeaderNavigationPage(driver);
        context.setAttribute("WebDriver", driver);
        userProfilePage = headerNavigation.getUserProfilePage();
    }

    @Test(priority=1)
    public void validateAllFieldsOnLoadDisabled() {
        try
        {
            Boolean isAllFieldsOnLoadDisabled = userProfilePage.isAllFieldsOnLoadDisabled();

            if (isAllFieldsOnLoadDisabled){
                String message = "Test Passed: All user profile fields on load disabled";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }

    }

    @Test(priority=2)
    public void validateErrorOnEmptyRequireFields() {
        try
        {
            Boolean isErrorDisplayedOnEmptyRequireFields = userProfilePage.isErrorDisplayedOnEmptyRequireFields();

            if (isErrorDisplayedOnEmptyRequireFields){
                String message = "Test Passed: All error displayed on empty required fields";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=3)
    public void validateEmailFieldNotEditable() {
        try {
            Boolean isEmailFieldNotEditable = userProfilePage.isEmailFieldNotEditable();

            if (isEmailFieldNotEditable) {
                String message = "Test Passed: Email field is not editable";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }

        } catch(Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority=4)
    public void validateAllEditableFieldsSaved() {
        try {
            Boolean isAllEditableFieldsSaved = userProfilePage.isAllEditableFieldsSaved();

            if (isAllEditableFieldsSaved) {
                String message = "Test Passed: All editable fields saved";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }

        } catch(Exception ex) {
            fail(ex.getMessage());
        }
    }

    //This is test will validate that
    @Test(priority=5)
    public void validatePhoneNumberFormat() {
        try
        {
            Boolean isErrorDisplayedOnEmptyRequireFields = userProfilePage.isPageValidatePhoneNumberFormat();

            if (isErrorDisplayedOnEmptyRequireFields){
                String message = "Test Passed: All error displayed on empty required fields";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 6)
    public void validateLogOut() {
        try {
            Boolean logOut = userProfilePage.isLogOut();

            if (logOut) {
                String message = "Test Passed: Log Out was successful";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 7)
    public void validateCurrentContent() {
        try {
            Boolean isCurrentContentEditable = userProfilePage.isCurrentContentEditable("Evan Test");

            if (isCurrentContentEditable) {
                String message = "Test Passed: Changing current content";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 8)
    public void languagePicker() {
        try {
            Boolean isLanguagePickerEditable = userProfilePage.isLanguagePickerSave("Spanish");

            if (isLanguagePickerEditable) {
                String message = "Test Passed: Changing language saved";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 9)
    public void languageDeselectable() {
        try {
            Boolean isLanguageDeselectable = userProfilePage.isLanguageDeselectable("Spanish");

            if (isLanguageDeselectable) {
                String message = "Test Passed: Language is deselectable";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 10)
    public void languageNumUpdatable() {
        try {
            Boolean isLanguageNumUpdatable = userProfilePage.islangaugeNumberUpdatable("Spanish", "2");

            if (isLanguageNumUpdatable) {
                String message = "Test Passed: Language number updated";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority = 11)
    public void selectAllLanguages() {
        try {
            Boolean isAllLanguagesSelectable = userProfilePage.selectAllLanguages();

            if (isAllLanguagesSelectable) {
                String message = "Test Passed: All languages selectable";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}




