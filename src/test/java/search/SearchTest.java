package search;
import extentreportlistener.TestNGListener;
import org.testng.ITestContext;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

import pages.*;
import base.TestBase;

import java.lang.reflect.Method;

@Listeners(TestNGListener.class)
public class SearchTest extends TestBase {
    Method method;
    ITestContext context;
    SearchPage searchPage;
    HeaderNavigationPage headerNavigation;
    String searchSuggestionKeyWord = "test";
    String searchResultKeyWord = "test";
    String myCollectionName = "My Test Collection";

    public SearchTest(){
        super();
    }

    @BeforeMethod
    public void setUp(Method m, ITestContext c) throws Exception {
        method = m;
        context = c;
        String methodName = method.getName();
        initialization("SearchTestAutomation", methodName);
        headerNavigation = new HeaderNavigationPage(driver);
        context.setAttribute("WebDriver", driver);
    }

    @Test(priority=1)
    public void getSearchSuggestions() throws Exception{

        try
        {
            searchPage = headerNavigation.getSearchPage();

            Boolean isSuggestDisplayed = searchPage.getSearchSuggestListMatchByKeyWord(searchSuggestionKeyWord);

            if (isSuggestDisplayed)
            {
                String message = "All suggestions contains keyword '" + searchSuggestionKeyWord + "'";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=2)
    public void getSearchResultByPressingEnterKey() throws Exception{

        try
        {
            searchPage = headerNavigation.getSearchPage();

            Boolean isSearchResultItemsMatchKeyWord = searchPage.getSearchResultListMatchByKeyWordByPressingEnter(searchResultKeyWord);

            if (isSearchResultItemsMatchKeyWord)
            {
                String message = "All search result item match search keyword '" + searchResultKeyWord + "'";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=3)
    public void getSearchResultByClickSubmitButton() throws Exception{
        try
        {
            searchPage = headerNavigation.getSearchPage();

            Boolean isSearchResultItemsMatchKeyWord = searchPage.getSearchResultListMatchByKeyWordByClickingSubmitButton(searchResultKeyWord);

            if (isSearchResultItemsMatchKeyWord)
            {
                String message = "All search result item match search keyword '" + searchResultKeyWord + "'";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=4)
    public void addSearchResultItemToFavorite() throws Exception{

        try
        {
            searchPage = headerNavigation.getSearchPage();

            Boolean isItemInFavorite = searchPage.addSearchResultItemToFavorite(searchResultKeyWord);

            if (isItemInFavorite)
            {
                String message = "Add search result item was added to favorite successfully";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
            else
            {
                fail("Add search result item add to favorite failed.");
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=5, dependsOnMethods = "addSearchResultItemToFavorite")
    public void removeSearchResultItemFromFavorite() throws Exception{
        try
        {
            searchPage = headerNavigation.getSearchPage();

            Boolean isItemInFavorite = searchPage.removeSearchResultItemFromFavorite(searchResultKeyWord);

            if (!isItemInFavorite)
            {
                String message = "Remove search result item from favorite was successful";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }else{
                fail("Remove search result item from favorite failed.");
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=6)
    public void addSearchResultItemToBasket() throws Exception{

        try
        {
            searchPage = headerNavigation.getSearchPage();

            Boolean isItemInBasket = searchPage.addSearchResultItemToBasket(searchResultKeyWord);

            if (isItemInBasket)
            {
                String message = "Add search result item to basket was successful";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }else{
                fail("Add search result item to basket failed.");
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=7, dependsOnMethods = "addSearchResultItemToBasket")
    public void removeSearchResultItemFromBasket() throws Exception{
        try
        {
            //Get search page
            searchPage = headerNavigation.getSearchPage();

            //Remove from basket first search item result by key word and get back media file name that we try to add and then remove
            Boolean isItemInBasket = searchPage.removeSearchResultItemFromBasket(searchResultKeyWord);

            //Verify if media file name not exists the test pass otherwise fail
            if (!isItemInBasket)
            {
                //Specify success message that we will display in browserstack with test name that we will handle in TestNGListener in onTestSuccess method
                String message = "Remove search result item from basket was successful";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }else{
                //Specify fail message that we will display in browserstack that we will handle in TestNGListener in onTestFailure method
                fail("Remove search result item from basket failed.");
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=8)
    public void addSearchResultItemToCollection() throws Exception{
        try
        {
            //First we clear all items from collection that we specified in myCollectionName variable;
            MyCollectionsPage collectionPage = headerNavigation.getMyCollectionsPage();
            collectionPage.clearCollectionMediaByName(myCollectionName);

            //Get search page
            searchPage = headerNavigation.getSearchPage();

            //Add first search result item to collection and get media file name
            String mediaFileName = searchPage.addSearchResultItemToCollection(searchResultKeyWord, myCollectionName);

            //Check media file name is empty if empty then test fail
            if (mediaFileName == null || mediaFileName.isEmpty()){
                //Specify fail message that we will display in browserstack that we will handle in TestNGListener in onTestFailure method
                fail("Add search result item to collection failed");
            }

            //Get collection page
            collectionPage = headerNavigation.getMyCollectionsPage();
            //Check if media file name exist in collections with collection name in variable myCollectionName that we will pass to function
            Boolean isItemInCollection = collectionPage.isItemInCollection(mediaFileName, myCollectionName);

            //verify return boolean if true the item exists and test pass if false then test fail
            if (isItemInCollection)
            {
                //Specify success message that we will display in browserstack with test name that we will handle in TestNGListener in onTestSuccess method
                String message = "Add search result item '" + mediaFileName + "' to collection " + myCollectionName + " was successful";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }else{
                //Specify fail message that we will display in browserstack that we will handle in TestNGListener in onTestFailure method
                fail("Add search result item '" + mediaFileName + "' to collection " + myCollectionName + " failed.");
            }
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @Test(priority=9)
    public void shareSearchResultItem() {
        searchPage = headerNavigation.getSearchPage();

        //get first media file from search result
        try
        {
            String mediaFileName = searchPage.shareSearchResultItem(searchResultKeyWord);
            //Specify success message that we will display in browserstack with test name that we will handle in TestNGListener in onTestSuccess method
            String message = "Share item '" + mediaFileName + " was successful";
            context.setAttribute(method.getName(), message);
            assertTrue(true);
        }
        catch(Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
