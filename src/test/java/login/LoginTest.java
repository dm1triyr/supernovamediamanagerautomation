package login;

import extentreportlistener.TestNGListener;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;

import pages.HomePage;
import pages.LoginPage;
import base.TestBase;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;

import org.testng.annotations.BeforeMethod;
import util.TestUtil;

@Listeners(TestNGListener.class)
public class LoginTest extends TestBase {
    Method method;
    ITestContext context;
    LoginPage loginPage;
    HomePage homePage;
    String validLoginUserName;
    String validLoginPassword;
    String invalidLoginPassword;
    String invalidLoginEmailAddress;
    String gmailTestEmail;


    public LoginTest(){
        super();
    }

    @BeforeMethod
    public void setUp(Method m, ITestContext c) throws Exception {
        method = m;
        context = c;
        String methodName = method.getName();
        validLoginUserName = prop.getProperty("TestUserName");
        validLoginPassword = prop.getProperty("TestPassword");
        invalidLoginEmailAddress = prop.getProperty("TestInvalidEmailAddress");
        invalidLoginPassword = prop.getProperty("TestInvalidPassword");
        gmailTestEmail = prop.getProperty("gmailTestAccountEmail");
        initialization("LoginTestAutomation", methodName);
        loginPage = new LoginPage(driver);
        context.setAttribute("WebDriver", driver);
    }

    @Test(priority=1)
    public void testClientLoginWithInvalidEmail() {
        try {
            Boolean errorDisplayed = loginPage.validateInvalidEmailAddress(invalidLoginEmailAddress);

            if (errorDisplayed) {
                String errorMessage = loginPage.getInvalidEmailErrorMessage();
                //BrowseStack create screenshot, so we don't need this method any more just for locally testing
                //TestUtil.takeScreenshotAtEndOfTest(driver);
                String message = "Test Passed: user was not able login with invalid email and user get error message '" + errorMessage + "'";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority=2)
    public void testClientLoginWithInvalidPassword() {
        try {
            Boolean errorDisplayed = loginPage.validateInvalidPassword(validLoginUserName, invalidLoginPassword);

            if (errorDisplayed) {
                String errorMessage = loginPage.getInvalidPasswordErrorMessage();
                //BrowseStack create screenshot, so we don't need this method any more just for locally testing
                //TestUtil.takeScreenshotAtEndOfTest(driver);
                String message = "Test Passed: user was not able login with invalid password and user got error message '" + errorMessage + "'";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority=3)
    public void testClientLoginRequiredUserName() {
        try {
            Boolean errorDisplayed = loginPage.validateRequiredLoginUserName(validLoginUserName);

            if (errorDisplayed) {
                String errorMessage = loginPage.getInvalidEmailErrorMessage();
                //BrowseStack create screenshot, so we don't need this method any more just for locally testing
                //TestUtil.takeScreenshotAtEndOfTest(driver);
                String message = "Test Passed: page required username with error message '" + errorMessage + "'";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority=4)
    public void testClientLoginRequiredPassword() {
        try {
            Boolean errorDisplayed = loginPage.validateRequiredLoginPassword(validLoginUserName);

            if (errorDisplayed) {
                String errorMessage = loginPage.getInvalidPasswordErrorMessage();
                //BrowseStack create screenshot, so we don't need this method any more just for locally testing
                //TestUtil.takeScreenshotAtEndOfTest(driver);
                String message = "Test Passed: Page required password with error message '" + errorMessage + "'";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }


    @Test(priority=5)
    public void testClientLoginSuccess() {
        try {
            homePage = loginPage.login(validLoginUserName, validLoginPassword);

            boolean isUserAvatarFound = homePage.getUserAvatar();

            if (isUserAvatarFound) {
                String message = "Test was successful login page was passed and redirect to home page and user avatar was found.";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority=6)
    public void helpSignInAction() {
        try {
            Boolean errorDisplayed = loginPage.validateHelpSignInPopup();

            if(errorDisplayed) {
                String message = "Test Passed: help sign-in popup appeared";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority=7)
    public void helpSignInInvalidEmail() {
        try {
            Boolean errorDisplayed = loginPage.isHelpSignInInvalidEmail();

            if(errorDisplayed) {
                String message = "Test Passed: user unable to use invalid email";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test(priority=8)
    public void testEmailReceived() {
        try {
            Boolean validateEmail = loginPage.validateEmailReceived(gmailTestEmail);

            if (validateEmail) {
                String message = "Test Passed: email received";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }


    @Test(priority=9)
    public void ValidateResetPasswordRuleRestriction()
    {
        try
        {
            Boolean isResetPasswordRuleExist = loginPage.isResetPasswordRuleExist();

            if(isResetPasswordRuleExist)
            {
                String message = "Reset password rule restriction is exist and error message was displayed";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }

    }

    @Test(priority=10)
    public void ValidateExpiredLinkForResetPassword()
    {
        try
        {
            Boolean isResetPasswordLinkExpired = loginPage.isResetPasswordLinkExpired();

            if(isResetPasswordLinkExpired)
            {
                String message = "A server error message was displayed because the password link has expired";
                context.setAttribute(method.getName(), message);
                assertTrue(true);
            }
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }

    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }
}
