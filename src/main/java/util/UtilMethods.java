package util;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;

import base.TestBase;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UtilMethods {
    private static WebDriver driver;

    protected static final int ID = 1;
    protected static final int CLASS = 2;
    protected static final int LINKTEXT = 3;
    protected static final int XPATH = 4;
    protected static final int CSS = 5;
    protected static final int TAGNAME = 6;

    protected static final int VISIBLETEXT = 1;
    protected static final int VALUE = 2;
    protected static final int INDEX = 3;

    public UtilMethods(WebDriver driver){
        UtilMethods.driver = driver;
    }

    public UtilMethods() {
    }

    public static void wait(int timeOutInSeconds) {
        driver.manage().timeouts().implicitlyWait(timeOutInSeconds, TimeUnit.SECONDS);
    }

    public static Actions getAction() {
        return new Actions(driver);
    }

    public static WebElement getElement(int byStrategy, String locatorValue) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            By by = null;

            switch (byStrategy) {
                case ID:
                    by = By.id(locatorValue);
                    break;
                case CLASS:
                    by = By.className(locatorValue);
                    break;
                case LINKTEXT:
                    by = By.linkText(locatorValue);
                    break;
                case XPATH:
                    by = By.xpath(locatorValue);
                    break;
                case CSS:
                    by = By.cssSelector(locatorValue);
                    break;
                case TAGNAME:
                    by = By.tagName(locatorValue);
                    break;
            }

            return wait.until(ExpectedConditions.visibilityOfElementLocated(by));

        }catch (NoSuchElementException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static WebElement getElement(WebElement parentElement, int byStrategy, String locatorValue) {
        By by = null;

        try {
            switch (byStrategy)
            {
                case ID:
                    by = By.id(locatorValue);
                    break;
                case CLASS:
                    by = By.className(locatorValue);
                    break;
                case LINKTEXT:
                    by = By.linkText(locatorValue);
                    break;
                case XPATH:
                    by = By.xpath(locatorValue);
                    break;
                case CSS:
                    by = By.cssSelector(locatorValue);
                    break;
                case TAGNAME:
                    by = By.tagName(locatorValue);
                    break;
            }

            return parentElement.findElement(by);

        }
        catch (NoSuchElementException e) {
            e.printStackTrace();
            throw e;
        }
    }


    public static List<WebElement> getElements(int byStrategy, String locatorValue) {
        By by = null;
        try{
            switch (byStrategy) {
                case ID:
                    by = By.id(locatorValue);
                    break;
                case CLASS:
                    by = By.className(locatorValue);
                    break;
                case LINKTEXT:
                    by = By.linkText(locatorValue);
                    break;
                case XPATH:
                    by = By.xpath(locatorValue);
                    break;
                case CSS:
                    by = By.cssSelector(locatorValue);
                    break;
                case TAGNAME:
                    by = By.tagName(locatorValue);
                    break;
            }
            return driver.findElements(by);
        }
        catch (NoSuchElementException e) {
            e.printStackTrace();
            throw e;
        }
    }


    public static void mouseOver(int byStrategy, String locatorValue) {
        WebElement el =  getElement(byStrategy, locatorValue);

        if (el == null)
            return;

        getAction().moveToElement(el).perform();
    }

    public static void mouseOver(WebElement parentElement,int byStrategy, String locatorValue) {
        WebElement el =  getElement(parentElement, byStrategy, locatorValue);

        if (el == null)
            return;

        getAction().moveToElement(el).perform();
    }

    public static void sendKeysWithEvent(WebElement element, String text, String event) {
        element.sendKeys(text);
        switch(event) {
            case "keyup":
                new Actions(driver).keyDown(element, Keys.CONTROL).keyUp(element, Keys.CONTROL).perform();
                break;
            case "onfocus":
                element.click();
                break;
            case "keyupTAB":
                element.sendKeys(Keys.TAB);
                break;
            case "mouseover":
                new Actions(driver).moveToElement(element).perform();
                break;
        }
    }

    public static void clearTextBox(int byStrategy, String locatorValue) {
        WebElement el =  getElement(byStrategy, locatorValue);

        if (el == null)
            return;

        el.clear();
    }

    public static void textBox(int byStrategy, String locatorValue, String text) {
        WebElement el =  getElement(byStrategy, locatorValue);

        if (el == null)
            return;

        el.clear();
        el.sendKeys(text);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].focus(); arguments[0].blur(); return true", el);
    }
    //On the function we can send another parameter as Key
    public static void textBox(Keys key, int byStrategy, String locatorValue, String text) {
        WebElement el =  getElement(byStrategy, locatorValue);

        if (el == null)
            return;

        el.clear();
        el.sendKeys(text);
        el.sendKeys(key);
    }

    public static void clickJS(int byStrategy, String locatorValue) {
        WebElement el = getElement(byStrategy, locatorValue);

        if (el == null)
            return;

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", el);
    }

    public static void click(int byStrategy, String locatorValue) {
        WebElement el = getElement(byStrategy, locatorValue);

        if (el == null)
            return;

        el.click();
    }

    public static void click(WebElement parentElement, int byStrategy, String locatorValue) {
        WebElement el = getElement(parentElement, byStrategy, locatorValue);

        if (el == null)
            return;

        el.click();
    }

    public static String getTxt(int byStrategy, String locatorValue) {
        WebElement el = getElement(byStrategy, locatorValue);

        if (el == null)
            return null;

        return el.getText();
    }

    public static String getTxt(WebElement parentElement, int byStrategy, String locatorValue) {
        WebElement el = getElement(parentElement, byStrategy, locatorValue);

        if (el == null)
            return null;

        return el.getText();
    }

    public static String getTxtBoxValue(int byStrategy, String locatorValue) {
        WebElement el = getElement(byStrategy, locatorValue);

        if (el == null)
            return null;

        return el.getAttribute("value");
    }

    public static String getTxtBoxValue(WebElement parentElement, int byStrategy, String locatorValue) {
        WebElement el = getElement(parentElement, byStrategy, locatorValue);

        if (el == null)
            return null;

        return el.getAttribute("value");
    }

    public static void selectCheckBoxFromList(List<WebElement> allOptions, String valueToSelect) {
        for (WebElement option : allOptions) {
            if (valueToSelect.equals(option.getText())) {
                option.click();
                break;
            }
        }
    }

    public static Boolean isElementDisplayed(int byStrategy, String locatorValue)
    {
        WebElement el = getElement(byStrategy, locatorValue);

        if (el == null)
            return null;

        return el.isDisplayed();
    }

    public static Boolean isElementSelected(int byStrategy, String locatorValue) {
        WebElement el = getElement(byStrategy, locatorValue);

        if (el == null)
            return null;

        return el.isSelected();
    }

    public static Boolean isElementEnabled(int byStrategy, String locatorValue)
    {
        WebElement el = getElement(byStrategy, locatorValue);

        if (el == null)
            return null;

        return el.isEnabled();
    }


    public static void dropDown(int byStrategy, String locatorValue, int selectStrategy, Object strategyValue) throws NoSuchElementException {
        try {
            WebElement webElement = getElement(byStrategy, locatorValue);

            Select select = new Select(webElement);

            switch (selectStrategy) {
                case VISIBLETEXT:
                    System.out.println("case 1");
                    select.selectByVisibleText((String) strategyValue);
                    break;
                case VALUE:
                    System.out.println("case 2");
                    select.selectByValue((String) strategyValue);
                    break;
                case INDEX:
                    System.out.println("case 3");
                    select.selectByIndex((Integer) strategyValue);
                    break;
            }
        } catch (NoSuchElementException e) {

        }
    }

    public static List<WebElement> getSearchResultList(String keyWord, int selectStrategy, String locatorValue)  throws Exception
    {
        String searchResultUrl = TestBase.currentUrl + "#search-results?q=" + keyWord;
        driver.get(searchResultUrl);
        Thread.sleep(2000);

        return getElements(selectStrategy, locatorValue);

    }

    public static WebElement getListItemByLocationAndIndex(int selectStrategy, String locatorValue, int index)  throws Exception
    {
        List<WebElement> list = getElements(selectStrategy, locatorValue);

        if (list.size() > index){
            return list.get(index);
        }

        return null;
    }

    public static boolean elementHasClass(WebElement element, String className)
    {
        return element.getAttribute("class").contains(className);
    }

    public static void goTo(String url)
    {
        driver.navigate().to(url);
    }

    public static void refresh()
    {
        driver.navigate().refresh();
    }

    public static boolean isMediaFileExists(int byStrategy, String listLocatorValue, String mediaFileLocatorValue, String mediaFileName)
    {
        List<WebElement> list = getElements(byStrategy, listLocatorValue);

        if (list.size() == 0)
            return false;

        for (WebElement item : list) {
            WebElement el = getElement(item, byStrategy, mediaFileLocatorValue);

            if (el == null)
                return false;

            String fileName = el.getText();

            if (fileName == null || fileName.isEmpty())
                return false;

            if (fileName.equals(mediaFileName))
                return true;

        }

        return false;
    }

    public static boolean verifyCopiedText(String expectedCopiedTex) throws IOException, UnsupportedFlavorException {

        //Verify that expected text was copied in clipboard
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        String actualCopedText = (String) clipboard.getData(DataFlavor.stringFlavor);
        System.out.println("String from Clipboard:" + actualCopedText);
        //Verify if expected text was copied
        return actualCopedText.equals(expectedCopiedTex);
    }

    public static void waitUntil(long timeoutms)  {
        long start = System.currentTimeMillis();
        Boolean meetCondition = false;
        while (!meetCondition){
            if (System.currentTimeMillis() - start > timeoutms ){
                meetCondition = true;
            }
        }
    }

    public void isBasketCounterDisplaying(String basketIconCounterCLASS) throws Exception {
        String getBasketCounter = getElement(CLASS, basketIconCounterCLASS).getText();
        String expectedBasketCount = "1";
        if (!getBasketCounter.equalsIgnoreCase(expectedBasketCount)) {
            throw new Exception("Fail: Basket counter [ Expected: " + expectedBasketCount + " ] -  [ Actual: " + getBasketCounter + " ]");
        }
    }


}