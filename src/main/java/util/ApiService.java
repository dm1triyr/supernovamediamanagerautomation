package util;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.io.*;
import javax.json.*;

public class ApiService {
    private final OkHttpClient httpClient = new OkHttpClient();

    public String RefreshUserToken(String localStorageAuthenticationProfiles, String userName, String password, String clientId, String clientSecret) throws Exception {
        String token;

        String refreshedAuthenticationProfile;
        // form parameters
        httpClient.newBuilder().readTimeout(10000, TimeUnit.MILLISECONDS)
                .connectTimeout(10000, TimeUnit.MILLISECONDS)
                .writeTimeout(10000, TimeUnit.MILLISECONDS)
                .build();

        RequestBody formBody = new FormBody.Builder()
                .add("username", userName)
                .add("password", password)
                .add("client_id", clientId)
                .add("client_secret", clientSecret)
                .add("grant_type", "password")
                .add("origin", "sro")
                .build();

        Request request = new Request.Builder()
                .url("https://auth.appdataroom.com/oauth/token")
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .post(formBody)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {

            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            // Get response body
            assert response.body() != null;
            String responseBody = response.body().string();
            JSONObject jsonResponseObject = new JSONObject(responseBody);
            token = jsonResponseObject.get("access_token").toString();
            StringReader reader = new StringReader(localStorageAuthenticationProfiles);
            JsonReader jsonReader = Json.createReader(reader);
            JsonObject jsonObject = jsonReader.readObject();
            String value = "";
            String jKey = "";
            for (String key: jsonObject.keySet()) {
                jKey = key;
                JSONObject tObject = new JSONObject(jsonObject.get(key).toString());
                tObject.remove("access_token");
                tObject.put("access_token", token);
                value = tObject.toString();
            }

            refreshedAuthenticationProfile = "{\"" + jKey + "\":" + value + "}";
        }

        return refreshedAuthenticationProfile;
    }

}
