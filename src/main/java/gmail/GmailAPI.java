package gmail;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.StringUtils;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.linkedin.urls.Url;
import com.linkedin.urls.detection.UrlDetector;
import com.linkedin.urls.detection.UrlDetectorOptions;
import org.apache.commons.codec.binary.Base64;
import java.io.IOException;
import java.io.*;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;



public class GmailAPI {
    /** Application name. */
    private static final String APPLICATION_NAME = "GmailApi";
    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    /** Directory to store authorization tokens for this application. */
    private static final String TOKENS_DIRECTORY_PATH = System.getProperty("user.dir") + "/resources/gmailcredentials/tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(GmailScopes.MAIL_GOOGLE_COM);
    private static final String CREDENTIALS_FILE_PATH = System.getProperty("user.dir") + "/resources/gmailcredentials/gmailCredentials.json";
    private static File filePath = new File(System.getProperty("user.dir") + "/resources/gmailcredentials/gmailCredentials.json");


    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = new FileInputStream(filePath); // Read credentials.json
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));


        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");

        final Long expiresIn = credential.getExpiresInSeconds();

        // trigger refresh if token is null or is about to expire
        if (credential.getAccessToken() == null
                || expiresIn != null && expiresIn <= 60) {
            try {
                credential.refreshToken();
            } catch (final IOException e) {

            }
        }

        return credential;
    }

    public static String getMailBody(String searchString) throws Exception {
        try {
            // Build a new authorized API client service.
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            // get list of messages with search criteria
            String user = "me";
            ListMessagesResponse listResponse = service.users().messages().list(user).setQ(searchString).execute();

            // Get ID of the email you are looking for
            String messageId = listResponse.getMessages().get(0).getId();

            Message message = service.users().messages().get(user, messageId).setFormat("FULL").execute();

            //Get email body of specific message
            String emailBody = StringUtils.newStringUtf8(Base64.decodeBase64(message.getPayload().getBody().getData()));

            return emailBody;
        } catch (Exception e) {
            throw new Exception("Can't find email in Google account with a search criteria  " + searchString + searchString + ". Error:" + e.getMessage());
        }
    }

    public static String getMailSubject(String searchString) throws Exception {
        try {
            String subject = "";
            // Build a new authorized API client service.
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            // get list of messages with search criteria
            String user = "me";
            ListMessagesResponse listResponse = service.users().messages().list(user).setQ(searchString).execute();

            // Get ID of the email you are looking for
            String messageId = listResponse.getMessages().get(0).getId();

            Message message = service.users().messages().get(user, messageId).setFormat("FULL").execute();

            JSONObject jsonObject = new JSONObject(message.toString());
            JSONObject payloadJsonObject = jsonObject.getJSONObject("payload");

            JSONArray headersJSONObject = payloadJsonObject.getJSONArray("headers");
            for (int i = 0; i < headersJSONObject.length(); ++i) {
                JSONObject j = headersJSONObject.getJSONObject(i);
                if (j.get("name").equals("Subject")) {
                    subject = j.get("value").toString();
                }
            }

            return subject;
        } catch (Exception e) {
            throw new Exception("Can't find email in Google account with a search criteria  " + searchString + searchString + ". Error:" + e.getMessage());
        }
    }

    public static String GetResetPasswordLink(String searchString) throws Exception {
        String resetPasswordLink = "";
        try {
            // Build a new authorized API client service.
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            // get list of messages with search criteria
            String user = "me";
            ListMessagesResponse listResponse = service.users().messages().list(user).setQ(searchString).execute();

            if (listResponse.size() == 0)
                throw new Exception("Can't find email in Google account with a search criteria  " + searchString);

            // Get ID of the email you are looking for
            String messageId = listResponse.getMessages().get(0).getId();

            Message message = service.users().messages().get(user, messageId).setFormat("FULL").execute();

            //Get email body of specific message
            String emailBody = StringUtils.newStringUtf8(Base64.decodeBase64(message.getPayload().getBody().getData()));

            UrlDetector parser = new UrlDetector(emailBody, UrlDetectorOptions.Default);
            List<Url> urls = parser.detect();

            if (urls.size() == 0)
                throw new Exception("Can't find reset password link inside email in Google account with a search criteria  " + searchString);

            resetPasswordLink = urls.get(0).getFullUrl();

            return resetPasswordLink;

        } catch (Exception e) {
            throw new Exception("Can't find email in Google account with a search criteria  " + searchString + ". Error:" + e.getMessage());
        }
    }

    public static boolean isMailExist(String searchString) throws Exception {
        try {
            // Build a new authorized API client service.
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            // get list of messages with search criteria
            String user = "me";
            ListMessagesResponse listResponse = service.users().messages().list(user).setQ(searchString).execute();

            return listResponse.size() != 0;

        } catch (Exception e) {
            throw new Exception("Can't find email in Google account with a search criteria  " + searchString + searchString + ". Error:" + e.getMessage());
        }
    }


}
