package pages;

import base.TestBase;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.regex.Matcher;

public class FavoritePage extends TestBase {
    private final WebDriver driver;

    private final String favoriteListElCLASS = "media-grid-item";
    private final String libraryBtnXPATH = "//*[@class=\"nav-actions\"]//div[1]";
    private final String dropDownBtnXPATH = "//*[@id=\"media_page_action_btn\"]";
    private final String addToFavoritesBtnXPATH = "//*[contains(text(), 'Add to Favorites')]";
    private final String removeFromFavoritesBtnXPATH = "//*[contains(text(), 'Remove from Favorites')]";
    private final String userDropdownID = "navbar_user_btn";
    private final String myFavoritesXPATH = " //*[contains(text(),'My Favorites')]";
    private final String hoverLikeXPATH = "//button[@class='btn-favorite rounded-btn fa fa-heart active']";
    private final String addToFavoritesPopupXPATH = "//*[contains(text(),' has been added as a ')]";
    private final String removeFromFavoritesPopupXPATH = "//*[contains(text(),' has been removed as a ')]";
    private final String undoBtnXPATH = "//button[@class='btn-undo']";

    private String dynamicContentXPATH = "//div[@class=\"title\"][contains(text(), '[contentName]')]/preceding-sibling::div";

    private String dynamicMediaTitleXPATH = "//div[@class=\"title\"]//span[text()='[mediaTitle]']";

    public FavoritePage(WebDriver driver) {
        this.driver = driver;
    }

    public void navToDynamicContent(String contentName) throws Exception {
        //nav to library
        click(XPATH, libraryBtnXPATH);
        //find dynamic element + click
        dynamicContentXPATH = dynamicContentXPATH.replace("[contentName]", contentName);
        click(XPATH, dynamicContentXPATH);
        Thread.sleep(200);
        //click dropdown
        click(XPATH, dropDownBtnXPATH);
        Thread.sleep(200); //need this sleep otherwise it will say element has "zero size"
        //click add to favorites
        click(XPATH, addToFavoritesBtnXPATH);
    }

    public String AddItemToFavoritesLocalStorage() throws Exception
    {
        String mediaTitle;
        String mediaItem = prop.getProperty("MediaItem");
        JSONObject jsonObject = new JSONObject(mediaItem);
        String userStuffState = localStorage.getItem("userstuffstate");
        mediaItem = mediaItem.replaceAll("\"", Matcher.quoteReplacement("\\\""));
        userStuffState = userStuffState.replace("\\\"favorites\\\":[]","\\\"favorites\\\":[" + mediaItem + "]");
        localStorage.setItem("userstuffstate", userStuffState);
        mediaTitle = jsonObject.get("title").toString();
        Thread.sleep(1000);
        refresh();

        return mediaTitle;
    }

    public Boolean isItemInFavorites(String mediaTitle)
    {
        dynamicMediaTitleXPATH = dynamicMediaTitleXPATH.replace("[mediaTitle]", mediaTitle);

        return getElements(XPATH, dynamicMediaTitleXPATH).size() > 0;
    }

    public Boolean isItemInLocalStorageFavorites(String mediaTitle){
        String rootJSONElement = prop.getProperty("TestUserName");
        String userStuffState = localStorage.getItem("userstuffstate");
        JSONObject jsonObject = new JSONObject(userStuffState);
        String rootJSON = jsonObject.getString(rootJSONElement);
        JSONObject rootJSONObject = new JSONObject(rootJSON);
        JSONArray favoritesArray = rootJSONObject.getJSONArray("favorites");
        for (int i = 0; i < favoritesArray.length(); ++i) {
            JSONObject j = favoritesArray.getJSONObject(i);
            String mediaFileInFavorite = j.get("title").toString();

            if (mediaTitle.equalsIgnoreCase(mediaFileInFavorite))
                return true;
        }

        return false;
    }

    public Boolean isItemInFavorites2(String contentName) throws Exception {
        //nav to dynamic content + add item to favorite
        navToDynamicContent(contentName);
        //click user dropdown
        click(ID, userDropdownID);
        //click favorites
        Thread.sleep(200); //need this sleep otherwise it will say element has "zero size"
        click(XPATH, myFavoritesXPATH);

        List<WebElement> listResults = getElements(CLASS, favoriteListElCLASS);
        boolean itemExists = false;
        for(WebElement results : listResults) {
            WebElement title = results.findElement(By.className("title"));
            String value = title.findElement(By.tagName("span")).getText();
            //validate title is the same
            if (contentName.toLowerCase().contains(value.toLowerCase())) {
                itemExists = true;
                break;
            }
        }

        return itemExists;
    }

    public Boolean removeItemFromFavorite(String contentName) throws Exception {
        //nav to dynamic content + add item to favorite
        navToDynamicContent(contentName);
        //click user dropdown
        click(ID, userDropdownID);
        //click favorites
        Thread.sleep(200); //need this sleep otherwise it will say element has "zero size"
        click(XPATH, myFavoritesXPATH);

        List<WebElement> listResults = getElements(CLASS, favoriteListElCLASS);
        for(WebElement results : listResults) {
            //hover over item and click like btn - to unlike it
            getAction().moveToElement(results).build().perform();
            mouseOver(XPATH, hoverLikeXPATH);
            Thread.sleep(200);
        }

        return true;
    }

    public Boolean isAddToFavoritesPopupDisplayed(String contentName) throws Exception {
        //nav to dynamic content + add item to favorite
        navToDynamicContent(contentName);

        //validate popup is displayed
        return getElement(XPATH, addToFavoritesPopupXPATH).isDisplayed();
    }

    public Boolean isRemoveFromFavoritesPopupDisplayed(String contentName) throws Exception {
        //nav to dynamic content + add item to favorite
        navToDynamicContent(contentName);
        //click dropdown
        click(XPATH, dropDownBtnXPATH);
        Thread.sleep(200); //need this sleep otherwise it will say element has "zero size"
        //click remove from favorites
        click(XPATH, removeFromFavoritesBtnXPATH);

        //validate popup is displayed
        return getElement(XPATH, removeFromFavoritesPopupXPATH).isDisplayed();
    }

    public Boolean undoAddToFavorites(String contentName) throws Exception {
        //nav to dynamic content + add item to favorite
        navToDynamicContent(contentName);
        //click dropdown
        click(XPATH, dropDownBtnXPATH);
        Thread.sleep(200); //need this sleep otherwise it will say element has "zero size"
        //click remove from favorites
        click(XPATH, removeFromFavoritesBtnXPATH);
        //click undo btn
        click(XPATH, undoBtnXPATH);

        return true;
    }

}
