package pages;

import base.TestBase;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;
import java.util.Locale;


public class SearchPage extends TestBase {
    private final WebDriver driver;
    HeaderNavigationPage headerNavigation;
    private final String searchTextBoxClass = "search-input";
    private final String searchResultItemClass = "search-results-item";
    private final String searchButtonClass = "rounded-btn";
    private final String searchSuggestionElClass = "suggestion";
    private final String btnFavoriteElClass = "btn-favorite";
    private final String actionMenuClass = "fa-ellipsis-v";
    private final String btnBasketElClass = "fa-shopping-bag";
    private final String btnCollectionElClass = "agenda";
    private final String btnShareNowElClass = "fa-share";
    private final String sharableLinkElID = "share_modal_shareable_link";
    private final String copySharableLinkElClass = "btn-copy";
    private final String searchResultItemTitleClass = "title";
    private final String searchResultItemTypeClass = "type";
    private final String sharePageMediaFileListElClass = "media-item";
    private final String sharePageMediaFileTitleElClass = "media-title";
    private final String firstSearchResultItemCSS = "div[class='search-results-item']>div[class='content']>div[class='text-and-actions']>div[class='text-content']>a";
    private final String firstSearchResultItemFavoriteButtonCSS = "div[class='search-results-item']>div[class='content']>div[class='text-and-actions']>div[class='actions']>div[class='tooltip-and-button-container']>button";

    private final By btnBasketAdd = By.xpath("//span[contains(text(),'Add To Basket')]");
    private final By btnBasketRemove = By.xpath("//span[contains(text(),'Remove From Basket')]");


    public SearchPage(WebDriver driver){
        this.driver = driver;
        headerNavigation = new HeaderNavigationPage(driver);
    }

    public Boolean getSearchSuggestListMatchByKeyWord(String keyWord) throws Exception {
        WebElement textBox = getElement(CLASS, searchTextBoxClass);

        if (textBox == null)
            throw new Exception("Search text box was not found");

        sendKeysWithEvent(textBox, keyWord, "keyup");
        Thread.sleep(2000);

        List<WebElement> listSuggestions = getElements(CLASS, searchSuggestionElClass);

        for(WebElement result: listSuggestions)
        {
            String value= result.getText();

            if(!value.toLowerCase().contains(keyWord.toLowerCase()))
                throw new Exception("Some suggestions not contains keyword '" + keyWord + "'");
        }

        return true;
    }

    public Boolean getSearchResultListMatchByKeyWordByPressingEnter(String keyWord) throws Exception {
        textBox(Keys.RETURN, CLASS, searchTextBoxClass, keyWord);
        Thread.sleep(2000);

        List<WebElement> listResults = getElements(CLASS, searchResultItemClass);

        if (listResults.size() == 0)
            throw new Exception("List search result is empty");

        for(WebElement result : listResults)
        {
            WebElement getItemTitle = getElement(result, CLASS, searchResultItemTitleClass);
            WebElement getItemType = getElement(result, CLASS, searchResultItemTypeClass);
            String itemType = getItemType != null ? getItemType.getText() : null;

            if (itemType != null && itemType.equalsIgnoreCase("pdf"))
                continue;

            String itemTitle = getItemTitle.getText();

            if(!itemTitle.toLowerCase().contains(keyWord.toLowerCase()))
            {
                //find media tag
                WebElement itemTagEl = getElement(result, CLASS, "tag");
                String itemTag = itemTagEl.getText();
                if(!itemTag.toLowerCase().contains(keyWord.toLowerCase()))
                    throw new Exception("Some search result item does not match search keyword '" + keyWord + "'");
            }
        }

        return true;
    }

    public Boolean getSearchResultListMatchByKeyWordByClickingSubmitButton(String keyWord) throws Exception {
        //Find element by CLASS searchTextBoxClass and Type text with passing variable keyWord in search textbox
        textBox(CLASS, searchTextBoxClass, keyWord);
        //Find button by CLASS searchButtonClass and Click search button to submit search request
        click(CLASS, searchButtonClass);
        //Wait 2 seconds to load search result
        Thread.sleep(2000);
        //Get search result list of elements
        List<WebElement> listResults = getElements(CLASS, searchResultItemClass);
        //Check if there is no elements return error
        if (listResults.size() == 0)
            throw new Exception("List search result is empty");

        //Check if there is elements then check if every item result match search key word
        for(WebElement result : listResults)
        {
            WebElement getItemTitle = getElement(result, CLASS, searchResultItemTitleClass);
            WebElement getItemType = getElement(result, CLASS, searchResultItemTypeClass);
            String itemType = getItemType != null ? getItemType.getText() : null;

            if (itemType != null && itemType.equalsIgnoreCase("pdf"))
                continue;

            String itemTitle = getItemTitle.getText();

            if(!itemTitle.toLowerCase().contains(keyWord.toLowerCase()))
            {
                //find media tag
                WebElement itemTagEl = getElement(result, CLASS, "tag");
                String itemTag = itemTagEl.getText();
                if(!itemTag.toLowerCase().contains(keyWord.toLowerCase()))
                    throw new Exception("Some search result item does not match search keyword '" + keyWord + "'");
            }
        }

        return true;
    }

    public Boolean addSearchResultItemToFavorite(String keyWord) throws Exception {
        String searchResultUrl = TestBase.currentUrl + "#search-results?q=" + keyWord;
        goTo(searchResultUrl);
        Thread.sleep(2000);

        WebElement el = getListItemByLocationAndIndex(CSS, firstSearchResultItemCSS, 0);

        if (el == null)
            throw new Exception("List search result is empty");

        WebElement btnFavorite = getListItemByLocationAndIndex(CSS, firstSearchResultItemFavoriteButtonCSS, 0);

        if (btnFavorite == null)
            throw new Exception("Favorite button not found");

        boolean alreadyInFavorite = btnFavorite.getAttribute("class").contains("active");

        if (!alreadyInFavorite){
            btnFavorite.click();
            Thread.sleep(2000);
        }

        String mediaFileInFavorite = el.getText();
        FavoritePage favoritePage = headerNavigation.getFavoritePage();

        return favoritePage.isItemInLocalStorageFavorites(mediaFileInFavorite) && favoritePage.isItemInFavorites(mediaFileInFavorite);
    }

    public Boolean removeSearchResultItemFromFavorite(String keyWord) throws Exception {
        String mediaFileInFavorite = null;
        List<WebElement> listResults = getSearchResultList(keyWord, CLASS, searchResultItemClass);

        //Check if there is no elements return error
        if (listResults.size() == 0)
            throw new Exception("List search result is empty");

        for(WebElement result : listResults)
        {
            WebElement btnFavorite = getElement(CLASS, btnFavoriteElClass);
            boolean alreadyInFavorite = btnFavorite.getAttribute("class").contains("active");

            if (!alreadyInFavorite){
                WebElement title = getElement(result, CLASS, searchResultItemTitleClass);
                mediaFileInFavorite = title.getText();
                btnFavorite.click();
                Thread.sleep(4000);
                break;
            }
        }
        FavoritePage favoritePage = headerNavigation.getFavoritePage();

        return !favoritePage.isItemInLocalStorageFavorites(mediaFileInFavorite) && !favoritePage.isItemInFavorites(mediaFileInFavorite);
    }

    public Boolean addSearchResultItemToBasket(String keyWord) throws Exception {
        String mediaFileInBasket = null;
        List<WebElement> listResults = getSearchResultList(keyWord, CLASS, searchResultItemClass);

        //Check if there is no elements return error
        if (listResults.size() == 0)
            throw new Exception("List search result is empty");

        for(WebElement result : listResults)
        {
            //Click on action menu for specific item search result
            click(result, CLASS, actionMenuClass);
            Thread.sleep(500);
            WebElement btnBasket = getElement(result, CLASS, btnBasketElClass);

            //Get parent element href of basket icon
            WebElement parent = btnBasket.findElement(By.xpath("./.."));
            //Get basket button span title
            String basketIconTitle = getTxt(parent,XPATH, "span");
            //Check if item already in basket if basket icon title will say "Remove From Basket"
            if (basketIconTitle.toLowerCase().contains("remove from basket")){
                //If item exists in basket then find another item not in basket
                continue;
            }

            btnBasket.click();
            Thread.sleep(2000);
            mediaFileInBasket = getTxt(result, CLASS, searchResultItemTitleClass);
            break;
        }

        BasketPage basketPage = headerNavigation.getBasketPage();

        return basketPage.isItemInLocalStorageBasket(mediaFileInBasket) && basketPage.isItemInBasket(mediaFileInBasket);
    }

    public Boolean removeSearchResultItemFromBasket(String keyWord) throws Exception {
        String mediaFileInBasket = null;
        List<WebElement> listResults = getSearchResultList(keyWord, CLASS,  searchResultItemClass);

        for(WebElement result : listResults)
        {
            //Click on action menu for specific item search result
            click(result, CLASS, actionMenuClass);
            Thread.sleep(500);

            WebElement btnBasket = getElement(result, CLASS, btnBasketElClass);
            if (btnBasket == null){
                continue;
            }
            WebElement parent = btnBasket.findElement(By.xpath("./.."));

            //Get basket button span title
            String basketIconTitle = getTxt(parent,XPATH, "span");
            //Check if item already in basket if basket icon title will say "Remove From Basket"
            if (basketIconTitle.toLowerCase().contains("remove from basket")){
                //If item exists in basket then click remove first
                btnBasket.click();
                Thread.sleep(2000);
            }else{
                continue;
            }

            mediaFileInBasket = getTxt(result, CLASS, searchResultItemTitleClass);

            break;
        }

        BasketPage basketPage = headerNavigation.getBasketPage();

        return !basketPage.isItemInLocalStorageBasket(mediaFileInBasket) && !basketPage.isItemInBasket(mediaFileInBasket);
    }

    public String addSearchResultItemToCollection(String keyWord, String myCollectionName) throws Exception{
        String value = null;
        List<WebElement> listResults = getSearchResultList(keyWord, CLASS, searchResultItemClass);

        for(WebElement result : listResults)
        {
            //Get media file name
            value = getTxt(result, CLASS, searchResultItemTitleClass);

            //Click on action menu for specific item search result
            click(result, CLASS, actionMenuClass);
            Thread.sleep(500);

            //Click Add to Collection icon
            click(result, CLASS, btnCollectionElClass);
            Thread.sleep(500);

            //Get Test Collection from Modal pop up
            List<WebElement> l = getElements(CLASS, "agenda-item");
            for(WebElement collectionItem : l){
                boolean isActive = elementHasClass(collectionItem, "active");
                List<WebElement> childs = collectionItem.findElements(By.xpath(".//div[contains(@class,'title')][text()='" + myCollectionName + "']"));

                if (childs.size() == 0)
                    continue;

                if (!isActive)
                    collectionItem.click();

                click(CLASS, "add-to-agenda");
                break;
            }
            break;
        }

        return value;
    }

    public String shareSearchResultItem(String keyWord) throws Exception {
        String fileName;
        String searchResultUrl = TestBase.currentUrl + "#search-results?q=" + keyWord;
        goTo(searchResultUrl);
        Thread.sleep(2000);

        WebElement item = getListItemByLocationAndIndex(CSS, firstSearchResultItemCSS, 0);

        if (item == null) {
            throw new Exception("Search result item was not found in search result list using keyword " + keyWord);
        }

        //Get media file name
        fileName = getTxt(item, CLASS, searchResultItemTitleClass);

        if (fileName == null || fileName.isEmpty())
            throw new Exception("Search result first item file name is empty using keyword " + keyWord);

        //Click on action menu for specific item search result item
        click(item, CLASS, actionMenuClass);
        Thread.sleep(500);

        //Click Share Now icon
        click(item, CLASS, btnShareNowElClass);
        Thread.sleep(500);

        //Get sharable link from text box
        String sharableLink = getTxtBoxValue(ID, sharableLinkElID);

        if (sharableLink == null || sharableLink.isEmpty())
            throw new Exception("Shareable Link text box is empty when user click Share Now icon on file " + fileName);

        //Click "Copy Link" to copy sharable link to clipboard
        click(CLASS, copySharableLinkElClass);
        Thread.sleep(500);

        boolean verifyCopyLink = verifyCopiedText(sharableLink);

        //Verify if copy share link to clipboard working
        if (!verifyCopyLink)
            throw new Exception(sharableLink + " was not found in clipboard when user click Copy link");

        // go to https://followups.gomodus.com/
        goTo(sharableLink);
        Thread.sleep(1000);

        //Verify if search result item that we are trying to share exists in sharable page https://followups.gomodus.com/
        if (!isMediaFileExists(CLASS, sharePageMediaFileListElClass, sharePageMediaFileTitleElClass, fileName))
            throw new Exception(fileName + " was not found in sharable page");


        return fileName;
    }
}