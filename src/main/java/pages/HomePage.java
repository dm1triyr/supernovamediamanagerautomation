package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends TestBase {
    private final WebDriver driver;
    HeaderNavigationPage headerNavigation;
    private final By userAvatar = By.className("user-avatar");
    private final By searchButton = By.className("fa-search");
    private String dynamicContentXPATH = "//div[@class=\"title\"][contains(text(), '[assetName]')]/preceding-sibling::div";
    private final String dropDownBtn = "//*[@id=\"media_page_action_btn\"]";
    private final String addToBasketBtnXPATH = "//*[contains(text(),'Add to Basket')]";
    private final String itemAddedPopupXPATH = "//*[contains(text(),'has been added')]";
    private final String removeFromBasketBtnXPATH = "//*[contains(text(),'Remove From Basket')]";
    private final String itemRemovedPopupXPATH = "//*[contains(text(),'has been removed')]";
    private final String basketIconCounterCLASS = "navbar-link-count";
    private final String contentCardDropdownXPATH = "//*[@class='btn btn-dropmenu btn-round fa fa-ellipsis-v tooltip-trigger']";


    public HomePage(WebDriver driver){
        this.driver = driver;
        headerNavigation = new HeaderNavigationPage(driver);
    }

    public Boolean getUserAvatar(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement avatar = wait.until(ExpectedConditions.visibilityOfElementLocated(userAvatar));
        return avatar.isDisplayed();
    }

    public SearchPage getSearchPage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement btn = wait.until(ExpectedConditions.visibilityOfElementLocated(searchButton));
        btn.click();

        return new SearchPage(driver);
    }

    public Boolean getSearchButton(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement btn = wait.until(ExpectedConditions.visibilityOfElementLocated(searchButton));

        return btn.isDisplayed();
    }

    public Boolean addAssetToBasket(String assetName) throws Exception {
        //we will find asset by asset name with dynamic XPATH
        dynamicContentXPATH = dynamicContentXPATH.replace("[assetName]", assetName);
        //hover + click add to basket first content item in first category
        mouseOver(XPATH, dynamicContentXPATH);
        //click three dots to see menu actions
        click(XPATH, contentCardDropdownXPATH);
        //find Add to basket action and click to add to basket
        click(XPATH, addToBasketBtnXPATH);
        //check if popup counter show up
        isBasketCounterDisplaying(basketIconCounterCLASS);

        //Use basket page and existing method to verify if asset exists in basket page
        BasketPage basketPage = headerNavigation.getBasketPage();
        waitUntil(1000);

        return basketPage.isItemInBasket(assetName);
    }

    public Boolean removeFromBasket(String mediaTitle) {
        click(XPATH, dropDownBtn);
        waitUntil(500); //need this sleep otherwise it will say element has "zero size"

        //validate "content has been removed" popup appears
        click(XPATH, removeFromBasketBtnXPATH);

        //use basket page function is isItemInLocalStorageBasket

        return true;
    }

}
