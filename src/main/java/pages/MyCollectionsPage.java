package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MyCollectionsPage extends TestBase {
    private final WebDriver driver;
    private final String collectionListClass = "agenda-item";
    private final String collectionMediaListClass = "media-grid-item";
    private final String btnDeleteClass = "btn-delete";
    private final String mediaMouseOverClass = "actions-overlay";
    private final String mediaTitleClass = "title";

    public MyCollectionsPage(WebDriver driver){
        this.driver = driver;
    }

    public Boolean isItemInCollection(String itemName, String myCollectionName) throws Exception {
        boolean itemExists = false;
        getCollectionByName(myCollectionName);

        List<WebElement> collections = getElements(CLASS, collectionMediaListClass);

        for(WebElement result : collections)
        {
            String fileName = getTxt(result, CLASS, mediaTitleClass);
            if (fileName.toLowerCase().contains(itemName.toLowerCase()))
            {
                itemExists = true;
                break;
            }
        }

        return itemExists;
    }

    public void getCollectionByName(String collectionName) throws Exception {
        boolean found = false;
        List<WebElement> collections = getElements(CLASS, collectionListClass);

        for(WebElement result : collections)
        {
            List<WebElement> childs = result.findElements(By.xpath(".//div[contains(@class,'description-container')][.//span[contains(@class,'title')][text()='My Test Collection']]"));

            if (childs.size() == 1){
                found = true;
                childs.get(0).click();
                Thread.sleep(500);
                break;
            }
        }

        if (!found){
            throw new Exception("Collection " +  collectionName + " not found");
        }
    }

    public void clearCollectionMediaByName(String collectionName) throws Exception {
        getCollectionByName(collectionName);

        List<WebElement> collections = getElements(CLASS, collectionMediaListClass);

        for(WebElement result : collections)
        {
            mouseOver(result, CLASS, mediaMouseOverClass);
            Thread.sleep(500);
            click(result, CLASS, btnDeleteClass);
            Thread.sleep(500);
        }
    }

}
