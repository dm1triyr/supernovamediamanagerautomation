package pages;

import base.TestBase;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.regex.Matcher;

public class BasketPage extends TestBase {
    private final WebDriver driver;
    private final By basketListEl = By.className("media-grid-item");

    private final String basketListCLASS = "//*[@class=\"media-grid-item\"]";
    private final String  libraryBtn = "//*[@class=\"nav-actions\"]//div[1]";
    private String dynamicContentXPATH = "//div[@class=\"title\"][contains(text(), '[mediaTitle]')]/preceding-sibling::div";
    private final String dropDownBtn = "//*[@id=\"media_page_action_btn\"]";
    private final String addToBasketBtnXPATH = "//*[contains(text(),'Add to Basket')]";
    private final String basketBtn = "//a[contains(@href,'#basket')]";
    private final String hoverDeleteXPATH = " //*[@class=\"btn-delete rounded-btn\"]";
    private String dynamicMediaTitleXPATH = "//div[@class=\"title\"]//span[text()='[mediaTitle]']";


    public BasketPage(WebDriver driver){
        this.driver = driver;
    }

    public String AddItemToBasketLocalStorage()
    {
        String mediaTitle;
        //read from config properties MediaItem string as JSON
        String mediaItem = prop.getProperty("MediaItem");
        //use JSON library that we add as dependency in pom file to parse JSON string as MediaItem json
        JSONObject jsonObject = new JSONObject(mediaItem);
        //read browser local storage with key "userstuffstate"
        String userStuffState = localStorage.getItem("userstuffstate");
        //add backslash to json string to save properly in local storage
        mediaItem = mediaItem.replaceAll("\"", Matcher.quoteReplacement("\\\""));
        userStuffState = userStuffState.replace("\\\"basket\\\":[]","\\\"basket\\\":[" + mediaItem + "]");
        //set MediaItem to browser local storage
        localStorage.setItem("userstuffstate", userStuffState);
        //media tiel from JSON user Json object
        mediaTitle = jsonObject.get("title").toString();
        refresh();

        return mediaTitle;
    }

    public Boolean isItemInBasket(String mediaTitle){
        dynamicMediaTitleXPATH = dynamicMediaTitleXPATH.replace("[mediaTitle]", mediaTitle);

        return getElements(XPATH, dynamicMediaTitleXPATH).size() > 0;
    }

    public Boolean isItemInLocalStorageBasket(String mediaTitle){
        //get TestUserName from config properties
        String rootJSONElement = prop.getProperty("TestUserName");
        //read browser local storage with key "userstuffstate"
        String userStuffState = localStorage.getItem("userstuffstate");
        //use JSON library that we add as dependency in pom file to parse JSON string as from local storage json
        JSONObject jsonObject = new JSONObject(userStuffState);
        //we know that root element in json always going to be user name so we use user name from config property and get root json string
        String rootJSON = jsonObject.getString(rootJSONElement);
        //using json root string we are getting root json object
        JSONObject rootJSONObject = new JSONObject(rootJSON);
        //get json array for basket
        JSONArray basketArray = rootJSONObject.getJSONArray("basket");
        //read all items in the json basket and check if mediaTitle that was passed function exists
        for (int i = 0; i < basketArray.length(); ++i) {
            JSONObject j = basketArray.getJSONObject(i);
            String mediaFileInBasket = j.get("title").toString();

            if (!mediaTitle.equalsIgnoreCase(mediaFileInBasket)) {
                continue;
            }
            return true;
        }

        return false;
    }


    public Boolean itemRemovedHover(String mediaTitle) throws Exception {
        dynamicMediaTitleXPATH = dynamicMediaTitleXPATH.replace("[mediaTitle]", mediaTitle);
        WebElement mediaFile = getElements(XPATH, dynamicMediaTitleXPATH).get(0);
        String title = mediaFile.getAttribute("innerHTML");
        if (!title.equals(mediaTitle)){
            throw new Exception("Test Fail: media file " + mediaTitle + " was not found");
        }
        WebElement parentElement = getElement(CLASS,"grid-item");
        mouseOver(parentElement,CLASS,"content");
        getAction().moveToElement(getElement(XPATH, hoverDeleteXPATH)).click().build().perform();
        waitUntil(1000);

        return getElements(XPATH, dynamicMediaTitleXPATH).size() == 0;
    }
}
