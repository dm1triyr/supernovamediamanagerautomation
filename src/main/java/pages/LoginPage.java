package pages;

import base.TestBase;
import com.google.api.services.gmail.GmailScopes;
import gmail.GmailAPI;
import org.apache.poi.sl.usermodel.TextBox;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoginPage extends TestBase {
    private final WebDriver driver;

    private final By errorMessage = By.className("toast-message");

    private final String usernameID = "username";
    private final String passwordID = "password";
    private final String groupModalCLASS = "supernova-modal-content";
    private final String groupToSelectXPATH = "//span[contains(text(),'Content Examples')]";
    private final String loginButtonCLASS = "btn-login";
    private final String errorMessageCLASS = "toast-message";
    private final String errorContainerCLASS = "toast-error";
    private final String needHelpBtnCLASS = "forgot-password-link";
    private final String forgotPasswordPopupCLASS = "modal-forgot-password";
    private final String resetEmailInputCLASS = "forgot-password-input";
    private final String resetPassBtnCLASS = "btn-modus";
    private final String resetPasswordID = "password-1";
    private final String repeatPasswordID = "password-2";
    private final String invalidResetPassword = "testing";
    private final String resetPasswordButtonCLASS = "reset-password-button";
    private final String errorMessageID = "warning-modal-dialog";
    private final String validResetPassword = "testing123123";
    private final String getErrorTextID = "warning-modal-text";


    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage login(String userName, String password) throws Exception {
        //click intro login
        click(CLASS, loginButtonCLASS);
        //enter username
        textBox(ID, usernameID, userName);
        //click login
        click(CLASS, loginButtonCLASS);
        //enter password
        textBox(ID, passwordID, password);
        //click login
        click(CLASS, loginButtonCLASS);

        WebElement element = getElement(XPATH, groupToSelectXPATH);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
        Thread.sleep(2000);

        String activeAccount = localStorage.getItem("activeAccount");
        String authenticationProfiles = localStorage.getItem("authenticationProfiles");
        String storedGroup = localStorage.getItem("storedGroup");

        if (activeAccount != null) {
            prop.setProperty("LocalStorageActiveAccount", activeAccount);
        }
        if (authenticationProfiles != null) {
            prop.setProperty("LocalStorageAuthenticationProfiles", authenticationProfiles);
        }
        if (activeAccount != null) {
            prop.setProperty("LocalStorageStoredGroup", storedGroup);
        }

        FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + "/src/main/java/config/config.properties");
        prop.store(fos, null);
        fos.close();

        return new HomePage(driver);
    }

    public Boolean validateInvalidEmailAddress(String userName) {
        //click intro login
        click(CLASS, loginButtonCLASS);
        //enter username
        textBox(ID, usernameID, userName);
        //click login
        click(CLASS, loginButtonCLASS);
        //check error message is displayed
        return isElementDisplayed(CLASS, errorMessageCLASS);
    }

    public Boolean validateInvalidPassword(String userName, String password) {
        //click intro login
        click(CLASS, loginButtonCLASS);
        //enter username
        textBox(ID, usernameID, userName);
        //click login
        click(CLASS, loginButtonCLASS);
        //enter password
        textBox(ID, passwordID, password);
        //click login
        click(CLASS, loginButtonCLASS);
        //check error is displayed
        return isElementDisplayed(CLASS, errorMessageCLASS);
    }

    public Boolean validateRequiredLoginUserName(String userName) {
        //click intro login
        click(CLASS, loginButtonCLASS);
        //enter username
        textBox(ID, usernameID, userName);
        //click login
        click(CLASS, loginButtonCLASS);
        clearTextBox(ID, usernameID);
        //click login
        click(CLASS, loginButtonCLASS);
        //check error is displayed
        return getElement(CLASS, errorMessageCLASS).isDisplayed();
    }

    public Boolean validateRequiredLoginPassword(String userName) {
        //click intro login
        click(CLASS, loginButtonCLASS);
        //enter username
        textBox(ID, usernameID, userName);
        //click login
        click(CLASS, loginButtonCLASS);
        clearTextBox(ID, usernameID);
        //click login
        click(CLASS, loginButtonCLASS);
        //check error is displayed
        return isElementDisplayed(CLASS, errorMessageCLASS);
    }

    public String getInvalidEmailErrorMessage() {
        WebElement modal = getElement(CLASS, errorContainerCLASS);

        return modal.findElement(errorMessage).getText();
    }

    public String getInvalidPasswordErrorMessage() {
        WebElement modal = getElement(CLASS, errorContainerCLASS);

        return modal.findElement(errorMessage).getText();
    }

    public Boolean validateHelpSignInPopup() {
        //click intro login
        click(CLASS, loginButtonCLASS);
        //click helpSignIn btn
        click(CLASS, needHelpBtnCLASS);
        //validate popup is displayed
        return isElementDisplayed(CLASS, forgotPasswordPopupCLASS);
    }

    public Boolean isHelpSignInInvalidEmail() throws Exception {
        //click intro login
        click(CLASS, loginButtonCLASS);
        //click helpSignIn btn
        click(CLASS, needHelpBtnCLASS);

        //email input field
        WebElement resetEmailField = getElement(CLASS, resetEmailInputCLASS);
        //list of invalid emails
        List<String> invalidEmails = new ArrayList<>(Arrays.asList("invalidEmail@test.com", "*#&#)#@&@", "jjj"));

        //send invalid emails and check for error message
        for (String email : invalidEmails) {
            resetEmailField.sendKeys(email);
            //click reset password btn
            click(CLASS, resetPassBtnCLASS);

            //check that error message is displayed
            if (!isElementDisplayed(CLASS, errorMessageCLASS)) {
                throw new Exception("Test Fail: error message was not displayed from invalid email attempt");
            }
            //clear email field
            resetEmailField.clear();
            Thread.sleep(800);
        }

        return true;
    }

    public Boolean validateEmailReceived(String testEmail) throws Exception {
        //click login first
        click(CLASS, loginButtonCLASS);
        //click helpSignIn btn
        click(CLASS, needHelpBtnCLASS);
        //input email and click reset password btn
        textBox(CLASS, resetEmailInputCLASS, testEmail);
        //click reset password btn
        click(CLASS, resetPassBtnCLASS);

        //gmail login and validate email
        //Give gmail 2 seconds to receive email
        Thread.sleep(800);
        //Call Gmail API class that will use Gmail APi to connect to test gmail base on configured credentials and check if email was receive
        //Test Gmail already have labeled gomodusresetpassword and filter that all incoming email with reset password will go this label (folder)
        return GmailAPI.isMailExist("label:gomodusresetpassword");
    }

    public Boolean isResetPasswordRuleExist() throws Exception {
        //Get reset password link visit Gmail API
        String ResetPasswordLink = GmailAPI.GetResetPasswordLink("label:gomodusresetpassword");
        //Navigate to reset password page
        goTo(ResetPasswordLink);
        //Enter invalid reset password
        textBox(ID, resetPasswordID, invalidResetPassword);
        //Enter repeat invalid reset password
        textBox(ID, repeatPasswordID, invalidResetPassword);
        //Click on reset password button
        click(CLASS, resetPasswordButtonCLASS);
        //Check that popup error message is displayed
        return isElementDisplayed(ID, errorMessageID);
    }

    public Boolean isResetPasswordLinkExpired() throws Exception {
        //Get reset password link visit Gmail API
        String ResetPasswordLink = GmailAPI.GetResetPasswordLink("label:gomodusexpiredresetpassword");
        //Navigate to reset password page
        goTo(ResetPasswordLink);
        //Enter invalid reset password
        textBox(ID, resetPasswordID, validResetPassword);
        //Enter repeat invalid reset password
        textBox(ID, repeatPasswordID, validResetPassword);
        //Click on reset password button
        click(CLASS, resetPasswordButtonCLASS);
        //Wait to load popup message
        Thread.sleep(3000);
        //Check that popup dialog with expired link error message is displayed
        return isElementDisplayed(ID, errorMessageID);
    }

}
