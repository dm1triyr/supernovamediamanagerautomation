package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HeaderNavigationPage extends TestBase {
    private final WebDriver driver;
    private final By myCollectionsLocation = By.xpath("//span[@class = 'label' and text()='My Collections']");
    private final By libraryLocation = By.xpath("//span[@class = 'label' and text()='Library']");
    private final By digitalSalesRoomLocation = By.xpath("//a[contains(@href,'#followup')]");
    private final By microlearningLocation = By.xpath("//a[contains(@href,'#microlearning')]");
    private final By notificationsLocation = By.xpath("//a[contains(@href,'#notifications')]");
    private final By basketLocation = By.xpath("//a[contains(@href,'#basket')]");
    private final By searchButtonLocation = By.xpath("//a[contains(@href,'#search')]");
    private final String userProfileAvatarLocation = "user-avatar";
    private final String viewProfileLocation = "//span[text()='View Profile']";

    public HeaderNavigationPage(WebDriver driver){
        this.driver = driver;
    }

    public HomePage getHomePage() {
        driver.get(currentUrl);
        waitUntil(2000);

        return new HomePage(driver);
    }

    public SearchPage getSearchPage() {
        String searchPageUrl = currentUrl.replace("#", "") + "#search";
        driver.get(searchPageUrl);
        waitUntil(2000);

        return new SearchPage(driver);
    }

    public MyCollectionsPage getMyCollectionsPage() {
        String searchPageUrl = currentUrl.replace("#", "") + "#collections";
        driver.get(searchPageUrl);
        waitUntil(2000);

        return new MyCollectionsPage(driver);
    }

    public LibraryPage getLibraryPage() {
        driver.get(currentUrl);
        waitUntil(2000);

        return new LibraryPage(driver);
    }

    public DigitalSalesRoomPage getDigitalSalesRoomPage() {
        String searchPageUrl = currentUrl.replace("#", "") + "#followup";
        driver.get(searchPageUrl);
        waitUntil(2000);

        return new DigitalSalesRoomPage(driver);
    }

    public MicrolearningPage getMicrolearningPage() {
        String searchPageUrl = currentUrl.replace("#", "") + "#microlearning";
        driver.get(searchPageUrl);
        waitUntil(2000);

        return new MicrolearningPage(driver);
    }

    public NotificationsPage getNotificationsPage() {
        String searchPageUrl = currentUrl.replace("#", "") + "#notifications";
        driver.get(searchPageUrl);
        waitUntil(2000);

        return new NotificationsPage(driver);
    }

    public BasketPage getBasketPage() {
        String searchPageUrl = currentUrl.replace("#", "") + "#basket";
        driver.get(searchPageUrl);
        waitUntil(2000);

        return new BasketPage(driver);
    }

    public FavoritePage getFavoritePage() {
        String favoriteUrl = currentUrl.replace("#", "") + "#favorites";
        driver.get(favoriteUrl);
        waitUntil(2000);

        return new FavoritePage(driver);
    }

    public UserProfilePage getUserProfilePage() {
       String userProfilePage = currentUrl + "#settings";
        driver.get(userProfilePage);
        waitUntil(2000);

        return new UserProfilePage(driver);
    }
}
