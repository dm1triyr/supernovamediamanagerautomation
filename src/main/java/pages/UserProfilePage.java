package pages;

import base.TestBase;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class UserProfilePage extends TestBase {
    private final WebDriver driver;
    HeaderNavigationPage headerNavigation;
    List<UserProfileFormField> lstFormFields;
    //XPATH to find all inputs in user profile form
    private final String formFieldsXPATH = "//form[@class='form']//input";
    //XPATH to find field label of giving field element
    private final String formFieldLabelXPATH = "preceding-sibling::span[1]";
    //XPATH to find field input by giving dynamic label
    private String formFieldInputXPATH = "//span[text() = '[label]']/following-sibling::input[1]";
    //XPATH to find "Edit Profile" button
    private final String editProfileBtnXPATH = "//button[@class='edit-button' and text() = 'Edit Profile']";
    //XPATH of required error message span element of input field element by giving dynamic label and dynamic error message
    private final String requiredErrorMessageXPATH = "//span[text() = '[label]']/following-sibling::span[text() = '[errorMessage]']";
    //XPATH of required error message span element of input field element by giving dynamic label
    private final String invalidFormatErrorMessageXPATH = "//span[text() = '[label]']/following-sibling::input[1]/following-sibling::*";
    //XPATH of setting btn
    private final String settingsXPATH = "//button[@class='tabs-control-tab-bar-item']/span[text() = 'Settings']";
    //XPATH of logOut btn
    private final String logOutXPATH = "//*[@class=\"section\"]//*[contains(text(),'Log Out')]";
    //CLASS of currentContext dropdown
    private final String currentContextCLASS = "updates-tab-form-item";
    //XPATH of current context popup from the settings page - dynamic label
    private String popupContextGroupNameXPATH = "//div[@class=\"group-select-modal-4\"]//span[contains(text(), '[groupName]')]";
    //XPATH of the current context dropdown from the homepage - dynamic label
    private String homePageCurrentContextGroupXPATH = "//button[@id='navbar_quick_groups_btn']/span[contains(.,'[groupName]')]";
    //XPATH language name - dynamic language
    private String languageNameXPATH = "//div[@class=\"language-select-modal-4\"]//div[contains(text(), '[language]')]/preceding::span[1]";
    //XPATH of language btn
    private final String languageBtnXPATH = "//div[@class=\"section\"]//div[contains(text(), 'Languages')]";
    //XPATH of ok btn
    private final String okBtnXPATH = "//button[@class=\"ok-btn\"]";
    //XPATH languages selected num - dynamic number
    private String languagesSelectedNumXPATH = "//*[contains(text(),'Languages')]/following-sibling::div[contains(text(),'[number]')]";
    //XPATH languages list
    private final String langList = "//div[@class=\"language-select-modal-4\"]//following-sibling::li";

    //because page has multiple buttons with the same class edit-button we will find button only by text
    private final String saveChangesProfileBtnXPATH = "//button[text() = 'Save Changes']";


    public UserProfilePage(WebDriver driver){
        this.driver = driver;
        headerNavigation = new HeaderNavigationPage(driver);
    }

    public Boolean isAllFieldsOnLoadDisabled() throws Exception {
        //Find all input fields in user profile form using UtilMethods getElements
        List<WebElement> fieldElements = getElements(XPATH, formFieldsXPATH);

        //Return error if user profile fields was not found
        if (fieldElements.size() == 0){
            throw new Exception("Can't find input fields with XPATH " + formFieldsXPATH);
        }

        //Loop through all user profile fields and check if all fields disabled on user profile load page
        for (WebElement item : fieldElements)
        {
            //Throw error if one of the user profile fields enable. Send error with user profile label using UtilMethods getTxt
            if (item.isEnabled())
                throw new Exception(getTxt(item, XPATH, formFieldLabelXPATH) + " enable on user profile load page");
        }
        return true;
    }

    public Boolean isErrorDisplayedOnEmptyRequireFields() throws Exception {
        //Initialize user profile field list that we will use later to check which field is required
        InitializeUserProfileFieldList();
        //Filter user profile fields to get only required fields
        Predicate<UserProfileFormField> byRequired = field -> field.Required;
        List<UserProfileFormField> filteredUserProfileFieldList = lstFormFields.stream().filter(byRequired)
                .collect(Collectors.toList());

        //Click on Edit Profile button to make form editable. We are using UtilMethods function
        click(XPATH, editProfileBtnXPATH);

        for (UserProfileFormField field : filteredUserProfileFieldList)
        {
            //set XPATH to find required field by field label
            String dynamicFormFieldInputXPATH = formFieldInputXPATH.replace("[label]", field.Label);
            //find required field in editable form and clear field
            clearTextBox(XPATH, dynamicFormFieldInputXPATH);
            //validate if required field error message displayed
            String dynamicRequiredErrorMessageXPATH = requiredErrorMessageXPATH.replace("[label]", field.Label).replace("[errorMessage]", field.RequiredErrorMessage);
            Boolean isErrorMessageDisplayed = isElementDisplayed(XPATH, dynamicRequiredErrorMessageXPATH);

            if (Boolean.FALSE.equals(isErrorMessageDisplayed))
                throw new Exception("Required error message was not displayed for " + field.Label);
        }

        return true;
    }

    public Boolean isPageValidatePhoneNumberFormat() throws Exception {
        //Initialize user profile field list that we will use later to check which field is required
        InitializeUserProfileFieldList();
        //Filter list to get phone field
        Predicate<UserProfileFormField> byRequired = field -> Objects.equals(field.Label, "Phone");
        UserProfileFormField phoneField = lstFormFields.stream().filter(byRequired)
                .collect(Collectors.toList()).get(0);

        //set XPATH to find required field by field label
        String dynamicFormFieldInputXPATH = formFieldInputXPATH.replace("[label]", phoneField.Label);
        //set XPATH to find invalid format error message
        String dynamicInvalidFormatErrorMessageXPATH = invalidFormatErrorMessageXPATH.replace("[label]", phoneField.Label);
        //set possible valid  phone numbers base on current phone format validation rule
        String[] possibleValidPhoneNumbers = new String[] { "0123456789", "(0123456789", "(012)3456789", "012-3456789", "012345-6789","012-345-6789","(012)345-6789" };
        //set possible invalid  phone numbers base on current phone format validation rule
        String[] possibleInvalidPhoneNumbers = new String[] { "012345678912", "012(3456789", "0123456789)", "0123-456789","abcd" };
        //Click on Edit Profile button to make form editable. We are using UtilMethods function
        click(XPATH, editProfileBtnXPATH);
        for (String phoneNumber : possibleValidPhoneNumbers)
        {
            //set phone number field with possible valid phone number using UtilMethods textBox methods
            textBox(XPATH, dynamicFormFieldInputXPATH, phoneNumber);
            //find next siblings after input which is span with error message
            List<WebElement> inputSiblingsList = getElements(XPATH, dynamicInvalidFormatErrorMessageXPATH);

            //If span more than 0 than check and span has error
            if (inputSiblingsList.size() > 0){
                //get first element from list
                WebElement el = inputSiblingsList.get(0);
                //if element tag span and error message equal message that was set in UserProfileFormField object and RequiredErrorMessage property then throw error
                if (Objects.equals(el.getTagName(), "span") && Objects.equals(el.getText(), phoneField.RequiredErrorMessage)){
                    throw new Exception("Invalid format error message was displayed for valid phone number " + phoneNumber);
                }
            }
        }

        for (String phoneNumber : possibleInvalidPhoneNumbers)
        {
            //set phone number field with possible valid phone number
            textBox(XPATH, dynamicFormFieldInputXPATH, phoneNumber);
            //find next siblings after input which is span with error message
            List<WebElement> inputSiblingsList = getElements(XPATH, dynamicInvalidFormatErrorMessageXPATH);

            //If span more than 0 than check and span has error
            if (inputSiblingsList.size() > 0){
                //get first element from list
                WebElement el = inputSiblingsList.get(0);
                //if element tag span and error message not equal message that was set in UserProfileFormField object and RequiredErrorMessage property then throw error
                if (Objects.equals(el.getTagName(), "span") && !Objects.equals(el.getText(), phoneField.RequiredErrorMessage)){
                    throw new Exception("Invalid format error message was not displayed for invalid phone number " + phoneNumber);
                }
            }
        }

        return true;
    }

    private static class UserProfileFormField {
        Boolean Required;
        Boolean Editable;
        String Label;
        String RequiredErrorMessage;
        String Value;

        private UserProfileFormField(Boolean required, Boolean editable, String label, String requiredErrorMessage, String value) {
            this.Required = required;
            this.Editable = editable;
            this.Label = label;
            this.RequiredErrorMessage = requiredErrorMessage;
            this.Value = value;
        }
    }

    //This was implement because it will be easy manage in the future if we will add more required and or more not editable fields
    //we can just add/remove from/to array and not change validation code

    //Initialize  lstFormFields variable with all fields
    private void InitializeUserProfileFieldList()
    {
        lstFormFields = Lists.newArrayList(
                new UserProfileFormField(true,true, "First Name", "This field is required", "autoFname"),
                new UserProfileFormField(true,true, "Last Name","This field is required", "autoLname"),
                new UserProfileFormField(false,true, "Company", "", "autoCompany"),
                new UserProfileFormField(false,true, "Position","", "autoPosition"),
                new UserProfileFormField(false,false, "Email","", ""),
                new UserProfileFormField(true,true, "Phone","Please enter a valid phone number (ex: 123-456-7890)", "234-743-9863")
        );
    }


    public Boolean isEmailFieldNotEditable() throws Exception {

        InitializeUserProfileFieldList();
        //Filter list to get email field / non editable field
        Predicate<UserProfileFormField> byEditable = field -> !field.Editable;

        List<UserProfileFormField> filteredNotEditableUserProfileFormField = lstFormFields.stream().filter(byEditable).collect(Collectors.toList());
        UserProfileFormField emailField = lstFormFields.stream().filter(byEditable).collect(Collectors.toList()).get(0);

        String dynamicFormFieldInputXPATH = formFieldInputXPATH.replace("[label]", emailField.Label);

        click(XPATH, editProfileBtnXPATH);

        if(isElementEnabled(XPATH, dynamicFormFieldInputXPATH))
            throw new Exception(emailField.Label + " textbox is supposed to be disabled but during the test, " + emailField.Label + " is editable on User Profile save.");
        //Validate if not editable field disabled on User Profile save mode

        return true;
    }


    public Boolean isAllEditableFieldsSaved() throws Exception {

        InitializeUserProfileFieldList();
        //list of all editable fields
        Predicate<UserProfileFormField> byEditable = field -> field.Editable;
        List<UserProfileFormField> byEditableFields = lstFormFields.stream().filter(byEditable).collect(Collectors.toList());
        //open edit
        click(XPATH, editProfileBtnXPATH);

        //loop through editable fields
        for (UserProfileFormField field : byEditableFields) {
            //set xpath to find editable fields
            String dynamicFormFieldInputXPATH = formFieldInputXPATH.replace("[label]", field.Label);
            //send data from UserProfileFormField array to form fields
            textBox(XPATH, dynamicFormFieldInputXPATH, field.Value);
        }

        //Lets give driver half second to find Save Changes button after we click Edit Profile button
        Thread.sleep(500);
        //I fix XPATH location and now its working
        click(XPATH, saveChangesProfileBtnXPATH);

        //refresh page by using new UtilMethod refresh() we trying to avoid use driver in the page and use UtilMethod where we have reference of driver object
        refresh();
        //Wait to load page with a new updated value
        Thread.sleep(1000);
        //open edit - Added another click edit profile to make easy to ready value from the textbox
        click(XPATH, editProfileBtnXPATH);
        Thread.sleep(500);

        //array of string that will contain all invalid text box Labels with current and expected values
        ArrayList<String> invalidTextBoxValueLabels = new ArrayList<>();

        //check if fields equal the userProfileFormField "value"
        for (UserProfileFormField field : byEditableFields) {
            String dynamicFormFieldInputXPATH = formFieldInputXPATH.replace("[label]", field.Label);

            //get current text box value using UtilMethod getTxtBoxValue
            String currentTextBoxValue = getTxtBoxValue(XPATH, dynamicFormFieldInputXPATH);

            //Validate if textbox value is not equal to the value that we tried previously saved, which is the value from UserProfileFormField Value property
            //if not equal then add textbox label to our invalidTextBoxValueLabels array
            if (!currentTextBoxValue.equals(field.Value)){
                invalidTextBoxValueLabels.add(field.Label + " value is " + currentTextBoxValue + " but expected " + field.Value);
            }
        }

        //Validate if array is not empty then convert array to comma separate string and throw error with error message
        if (invalidTextBoxValueLabels.size() > 0)
            throw new Exception("Here is a list of all fields that did not retain value after saving the page: " + String.join(",", invalidTextBoxValueLabels));

        return true;
    }

    public Boolean isLogOut() {
        //get current active account from local storage before log out
        String activeAccountBeforeLogout = localStorage.getItem("activeAccount");
        //click on Settings tab
        click(XPATH, settingsXPATH);
        //click on Log Out button
        click(XPATH, logOutXPATH);
        //get current active account from local storage before log out
        String activeAccountAfterLogout = localStorage.getItem("activeAccount");

        if (activeAccountBeforeLogout.equals(activeAccountAfterLogout))
            return false;

        return true;
    }

    public Boolean isCurrentContentEditable(String groupName) throws Exception {
        //set popup context xpath with dynamic groupName
        popupContextGroupNameXPATH = popupContextGroupNameXPATH.replace("[groupName]", groupName);
        //click on Settings tab
        click(XPATH, settingsXPATH);
        //click on current content button
        click(CLASS, currentContextCLASS);
        //click on context group name that was passed with test
        click(XPATH, popupContextGroupNameXPATH);
        //wait for page navigate to home page
        Thread.sleep(2000);
        //validating context group name change
        homePageCurrentContextGroupXPATH = homePageCurrentContextGroupXPATH.replace("[groupName]", groupName);
        //validate if group name that was selected displayed in home page left top corner
        if (!isElementDisplayed(XPATH, homePageCurrentContextGroupXPATH)) {
            return false;
        }

        return true;
    }

    public void navToLanguage() throws Exception {
        //click on Settings tab
        click(XPATH, settingsXPATH);
        //click on language popup
        click(XPATH, languageBtnXPATH);
        //click on language that was passed in
        click(XPATH, languageNameXPATH);
        //click ok btn
        click(XPATH, okBtnXPATH);
        Thread.sleep(200);
    }

    public Boolean isLanguagePickerSave(String language) throws Exception {
        //set language context xpath with dynamic language
        languageNameXPATH = languageNameXPATH.replace("[language]", language);
        //navigates and clicks on passed in language
        navToLanguage();

        //refresh page
        refresh();
        //click on Settings tab
        click(XPATH, settingsXPATH);
        //click on language popup
        click(XPATH, languageBtnXPATH);
        Thread.sleep(200);
        //set language
        String languageExpectedXPATH = languageNameXPATH.replace("[language]", language);

        //validate language selection saved
        if (!isElementSelected(XPATH, languageExpectedXPATH)) {
            return false;
        }

        return true;
    }

    public Boolean isLanguageDeselectable(String language) throws Exception {
        //set language context xpath with dynamic language
        languageNameXPATH = languageNameXPATH.replace("[language]", language);
        //navigates and clicks on passed in language
        navToLanguage();

        //refresh page
        refresh();
        //click on Settings tab
        click(XPATH, settingsXPATH);
        //click on language popup
        click(XPATH, languageBtnXPATH);
        //deselect language that was passed in
        click(XPATH, languageNameXPATH);

        //validate language is not selected
        if (isElementSelected(XPATH, languageNameXPATH)) {
            return false;
        }
        //click ok btn
        click(XPATH, okBtnXPATH);
        Thread.sleep(200);

        return true;
    }

    public Boolean islangaugeNumberUpdatable(String language, String number) throws Exception {
        //set language context xpath with dynamic language
        languageNameXPATH = languageNameXPATH.replace("[language]", language);
        //navigates and clicks on passed in language
        navToLanguage();

        //refresh page
        refresh();
        //click on Settings tab
        click(XPATH, settingsXPATH);
        //Added Wait for element because it failed test
        Thread.sleep(3000);
        languagesSelectedNumXPATH = languagesSelectedNumXPATH.replace("[number]", number);
        //check if passed in num matches languages selected num
        if (!isElementDisplayed(XPATH, languagesSelectedNumXPATH)) {
            return false;
        }

        return true;
    }

    public Boolean selectAllLanguages() throws Exception {
        //click on Settings tab
        click(XPATH, settingsXPATH);
        //click on language popup
        click(XPATH, languageBtnXPATH);

        //select all languages in the list
        List<WebElement> langaugesList = getElements(XPATH, langList);
        for (WebElement lang : langaugesList) {
            lang.click();
        }
        //click ok btn
        click(XPATH, okBtnXPATH);
        Thread.sleep(200);

        return true;
    }

}


