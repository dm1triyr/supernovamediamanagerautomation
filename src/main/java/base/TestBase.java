package base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteExecuteMethod;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.html5.RemoteWebStorage;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import util.ApiService;
import util.TestUtil;
import util.UtilMethods;
import util.WebEventListener;

public class TestBase extends UtilMethods {
    public static WebDriver driver;
    public static Properties prop;
    public  static EventFiringWebDriver e_driver;
    public static WebEventListener eventListener;
    public static String runWebDriverLocally;
    public static String currentUrl;
    public static LocalStorage localStorage;
    private static String LoginUrl;
    private static String environmentState;


    public TestBase(){
        super();
        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+ "/src/main/java/config/config.properties");
            prop.load(ip);
            runWebDriverLocally = System.getenv("RUN_WEBDRIVER_LOCALLY");
            LoginUrl = System.getenv("LOGIN_URL");
            environmentState = System.getenv("ENVIRONMENT_STATE");

            if(LoginUrl == null || LoginUrl.isEmpty())
                LoginUrl = prop.getProperty("LoginUrl");

            if(runWebDriverLocally == null || runWebDriverLocally.isEmpty())
                runWebDriverLocally = prop.getProperty("runLocally");

            if(environmentState == null || environmentState.isEmpty())
                environmentState = "LOCALL";

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initialization(String testClass, String testName) throws  Exception {
        String bsUserName = prop.getProperty("BROWSERSTACK_USERNAME");
        String bsApiKey = prop.getProperty("BROWSERSTACK_AUTOMATE_KEY");
        String localStorageActiveAccount = prop.getProperty("LocalStorageActiveAccount").replace("\\","");
        String localStorageAuthenticationProfiles = prop.getProperty("LocalStorageAuthenticationProfiles").replace("\\","");
        String localStorageStoredGroup = prop.getProperty("LocalStorageStoredGroup").replace("\\","");
        String userAccessTokenRefreshDate = prop.getProperty("UserAccessTokenRefreshDate").replace("\\","");
        String userName = prop.getProperty("TestUserName");
        String password = prop.getProperty("TestPassword");
        String clientId = prop.getProperty("ClientId");
        String clientSecret = prop.getProperty("ClientSecret");
        Boolean callRefreshUserToken = false;

        LocalDate today = LocalDate.now();
        int month = today.getMonthValue();
        int day = today.getDayOfMonth();
        int year = today.getYear();

        if (userAccessTokenRefreshDate.isEmpty()) {
            userAccessTokenRefreshDate = "2022-01-01";
        }

        DateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dtUserAccessTokenRefreshDate = inFormat.parse(userAccessTokenRefreshDate);
        Date currentDate = inFormat.parse(today.toString());
        Boolean isSameDay = DateUtils.isSameDay(dtUserAccessTokenRefreshDate, currentDate);
        //Refresh user token if last time refresh was not today
        if (isSameDay == false && !testClass.equals("LoginTestAutomation"))
        {
            ApiService apiService = new ApiService();
            String refreshToken = apiService.RefreshUserToken(localStorageAuthenticationProfiles, userName, password, clientId, clientSecret).replace("//","/");
            prop.setProperty("LocalStorageAuthenticationProfiles", refreshToken);
            prop.setProperty("UserAccessTokenRefreshDate", today.toString());
            localStorageAuthenticationProfiles = refreshToken;
            FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + "/src/main/java/config/config.properties");
            prop.store(fos, null);
            fos.close();
        }

        String bsUrl = "https://" + bsUserName + ":" + bsApiKey + "@hub-cloud.browserstack.com/wd/hub";

        if(runWebDriverLocally.equals("1"))
        {
            //System.setProperty("webdriver.chrome.driver", "resources/chromedriver/97/chromedriver.exe");
            WebDriverManager.chromedriver().clearResolutionCache().setup();
            driver = new ChromeDriver();
            localStorage = ((WebStorage) driver).getLocalStorage();
        }
        else
        {
            String buildNumber = System.getenv("BITBUCKET_BUILD_NUMBER");
            if (buildNumber == null)
                buildNumber = MessageFormat.format("{0}.{1}.{2}", month, day, year);

            String buildName = "CLIENT-APP." + environmentState + "." + buildNumber;
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setPlatform(Platform.WINDOWS);
            caps.setBrowserName("chrome");
            caps.setCapability("browser_version", "latest");
            caps.setCapability("browserstack.debug", "true");
            caps.setCapability("project", "Automation.Dev");
            caps.setCapability("build", buildName );
            caps.setCapability("name", testName + "." + testName);
            caps.setCapability("browserstack.console", "info");  // to enable console logs at the info level. You can also use other log levels here
            caps.setCapability("browserstack.networkLogs", "true");  // to enable network logs to be logged
            caps.setCapability("browserstack.idleTimeout", "500");
            driver = new RemoteWebDriver(new URL(bsUrl), caps);

            RemoteExecuteMethod executeMethod = new RemoteExecuteMethod((RemoteWebDriver) driver);
            RemoteWebStorage webStorage = new RemoteWebStorage(executeMethod);
            localStorage = webStorage.getLocalStorage();
        }

        e_driver = new EventFiringWebDriver(driver);
        // Now create object of EventListerHandler to register it with EventFiringWebDriver
        eventListener = new WebEventListener();
        e_driver.register(eventListener);
        driver = e_driver;

        driver.manage().window().maximize();
        //driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        try {
            URL baseUrl = new URL(LoginUrl);
            HttpURLConnection connection = (HttpURLConnection) baseUrl.openConnection();
            connection.connect();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Unable to connect to the url connection");
                System.out.println("Response Code " + connection.getResponseCode());
                System.out.println("Response Message " + connection.getResponseMessage());
            }

            driver.get(LoginUrl);
            waitUntil(2000);

            String activeAccount = localStorage.getItem("activeAccount");
            String authenticationProfiles = localStorage.getItem("authenticationProfiles");
            String storedGroup = localStorage.getItem("storedGroup");

            if (!testClass.equals("LoginTestAutomation"))
            {
                if (activeAccount == null){
                    localStorage.setItem("activeAccount", localStorageActiveAccount);
                }
                if (authenticationProfiles == null){
                    localStorage.setItem("authenticationProfiles", localStorageAuthenticationProfiles);
                }
                if (storedGroup == null){
                    localStorage.setItem("storedGroup", localStorageStoredGroup);
                }
                LoginUrl = LoginUrl.replace("login","");
                driver.get(LoginUrl);
                waitUntil(2000);
                driver.navigate().refresh();
            }

            new UtilMethods(driver);
            currentUrl = LoginUrl.replace("login","");
            String userStuffState = localStorage.getItem("userstuffstate");

            if (userStuffState == null){
                userStuffState = prop.getProperty("UserStuffStateLocalStorage");
                userStuffState = userStuffState.replace("[username]", prop.getProperty("TestUserName"));
                localStorage.setItem("userstuffstate", userStuffState);
            }

        } catch (IOException e) {
            System.out.println("URL connection error");
            e.printStackTrace();
        }
    }

    public static void sendToBrowseStackTestStatus(WebDriver driver, String status, String reason) {
        if(runWebDriverLocally.equals("0"))
        {
            try {
                JavascriptExecutor jse = (JavascriptExecutor)driver;
                jse.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\":\"" + status + "\", \"reason\": \"" + reason + "\"}}");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

